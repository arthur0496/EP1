#include <iostream>
#include "imagem.hpp"
#include "filtro.hpp"
#include "filtro_negativo.hpp"
#include "filtro_polarizado.hpp"
#include "filtro_black_white.hpp"
#include "filtro_media.hpp"

using namespace std;

int main(){
int opc,aux;
Imagem* imagem;
Filtro_Negativo *filtro_1;
Filtro_Polarizado *filtro_2;
Filtro_Black_White *filtro_3;
Filtro_Media *filtro_4;

do{
	imagem = new Imagem();
	imagem->abrir_arquivo();
	imagem->ler_arquivo();
	if(imagem->get_open()){
		cout<<"Escolha o filtro:"<<endl;
		cout<<"1 - Filtro negativo" << endl;
		cout<<"2 - Filtro polarizado"<< endl;
		cout<<"3 - Filtro preto e branco" << endl;
		cout<<"4 - Filtro madia" << endl;
		cin >> opc;
		cout << endl;
		if(opc==1){
			filtro_1 = new Filtro_Negativo(imagem->get_altura(),imagem->get_largura(),imagem->get_numero_magico(),imagem->get_pixel());

			filtro_1->aplicar_filtro();
			imagem->set_pixel(filtro_1->get_pixel());
			delete filtro_1;
		}	
		if(opc == 2){
			filtro_2 = new Filtro_Polarizado(imagem->get_altura(),imagem->get_largura(),imagem->get_numero_magico(),imagem->get_pixel());

			filtro_2->aplicar_filtro();
			imagem->set_pixel(filtro_2->get_pixel());
			delete filtro_2;
		}
		if(opc == 3){
		        filtro_3 = new Filtro_Black_White(imagem->get_altura(),imagem->get_largura(),imagem->get_pixel());
		        filtro_3->aplicar_filtro();
			imagem->set_pixel(filtro_3->get_pixel());
			delete filtro_3;
		}
		if(opc == 4){
			filtro_4 = new Filtro_Media(imagem->get_altura(),imagem->get_largura(),imagem->get_pixel());
		        filtro_4->aplicar_filtro();
		        imagem->set_pixel(filtro_4->get_pixel());
			delete filtro_4;
		}

		imagem->criar_novo_arquivo();
		imagem->escrever_novo_arquivo();
		delete imagem;
	}
	cout << "Deseja aplicar um filtro em outra imagem ?" << endl;
	cout << "1 - sim" << endl;
	cout << "0 - nao" << endl;
	cin >> aux;
	}while(aux);

	return 0;
}
