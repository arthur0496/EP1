#include<iostream>
#include<filtro.hpp>

using namespace std;

Filtro::Filtro(){
	altura = 0;
	largura = 0;
}

Filtro::Filtro(int altura,int largura, unsigned char*** pixel){
	this-> altura = altura;
	this-> largura = largura;
	this-> pixel = pixel;
}

void Filtro::aplicar_filtro(){
	int i,j,largura,altura;
        unsigned char*** pixel,grayscale_value;

        largura=get_largura();
        altura=get_altura();
        pixel=get_pixel();
        for(i=0;i<altura;i++){
                for(j=0;j<largura;j++){
                        grayscale_value = (0.299 * pixel[i][j][0]) + (0.587 * pixel[i][j][1]) + (0.144 * pixel[i][j][3]);
                        pixel[i][j][0] = grayscale_value;
                        pixel[i][j][1] = grayscale_value;
                        pixel[i][j][2] = grayscale_value;
                }
        }
        set_pixel(pixel);

}

int Filtro::get_altura(){
	return altura;
}

int Filtro::get_largura(){
	return largura;
}

unsigned char*** Filtro::get_pixel(){
	return pixel;
}

void Filtro::set_altura(int altura){
	this-> altura = altura;
}

void Filtro::set_largura(int largura){
        this-> largura = largura;
}

void Filtro::set_pixel(unsigned char*** pixel){
	this->pixel=pixel;
}
