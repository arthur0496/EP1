#include<iostream>
#include"filtro_media.hpp"

using namespace std;

Filtro_Media::Filtro_Media(int altura,int largura, unsigned char*** pixel){
        set_altura(altura);
        set_largura(largura);
	set_pixel(pixel);
}

void Filtro_Media::aplicar_filtro(){
	int opc,i,j,k,l,limite,largura,altura;
	unsigned char*** pixel;
	int valor_r=0,valor_g=0,valor_b=0;
	cout<<"Digite o tamanho da mascara que deseja usar:"<<endl;
	cin >> opc;
	cout << endl;
	pixel= get_pixel();
	largura = get_largura();
	altura = get_altura();
	if(opc%2){
		limite=opc/2;
		for(i=limite;i<altura-limite; i++){
			for(j=limite;j < largura - limite;j++){
				for(k=0-limite;k<opc-limite;k++){
					for(l=0-limite;l<opc-limite;l++){
						valor_r+=pixel[i+k][j+l][0];
						valor_g+=pixel[i+k][j+l][1];
						valor_b+=pixel[i+k][j+l][2]; 
					}		
				}
				valor_r /= (opc*opc);
				valor_g /= (opc*opc);
				valor_b /= (opc*opc);
				pixel[i][j][0]=valor_r;
        	                pixel[i][j][1]=valor_g;
	                        pixel[i][j][2]=valor_b;

			}
		}
	}
	else
		cout << "Mascara invalida, filtro nao aplicado" << endl;
}
