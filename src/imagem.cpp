#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstring>
#include "imagem.hpp"

using namespace std;

Imagem::Imagem(){
        altura = 0;
        largura = 0;
        numero_magico = 0;
	open=0;
}

Imagem::~Imagem(){
        arquivo.close();
        novo_arquivo.close();
        delete[] pixeldata;
	delete[] pixel;
	}

void Imagem::abrir_arquivo(){
	string nome;
        cout << "Digite o nome da imagem ppm que deseja aplicar o filtro:";
	cout <<"(sem extensao de formato)" << endl; 
        cin >> nome;
	nome="doc/" + nome + ".ppm";
        arquivo.open(nome.c_str());
        cout << endl;
	if(!arquivo.is_open())
                cout << "Falha ao abrir o arquivo." << endl;
	else
		open=1;
}

void Imagem::criar_novo_arquivo(){
	string nome;
	cout << "Digite o nome da imagem que sera criada";
	cout <<"(sem extensao de formato)" << endl;
	cin >> nome;
	nome = "doc/"+nome+".ppm";
	novo_arquivo.open(nome.c_str());
        if(!novo_arquivo.is_open())
                cout << "Falha ao abrir a imagem." << endl;
}

void Imagem::ler_arquivo(){
        string linha;
        int i=0,j=0,k=0;;

        while(i<3){
                getline(arquivo,linha);
                if(linha[0]!='#'){
                        if(i==1){
                                if(linha.length() > 4){
                                        stringstream s0(linha);
                                        s0 >> largura;
                                        s0 >> altura;
                                }
                                else{
                                        stringstream s1(linha);
                                        s1 >> largura;
                                        getline(arquivo,linha);
                                        stringstream s2(linha);
                                        s2 >> altura;
                                }
                        }
                        if(i==2){
                                stringstream s4(linha);
                                s4 >> numero_magico;
                        }
                        i++;
                }
        }

        tamanho = 3*altura*largura;
        pixeldata = new char[tamanho];
	pixel = new unsigned char**[altura];
	for(i=0;i<altura;i++){
		pixel[i] = new unsigned char*[largura];
		for(j=0;j<largura;j++){
			pixel[i][j] = new unsigned char [3];
		}
	}
        arquivo.read(pixeldata,tamanho);
	j=0;
	for(i=0;i<tamanho;i+=3){
		pixel[k][j][0] = pixeldata[i];
		pixel[k][j][1] =pixeldata[i+1];
		pixel[k][j][2] =pixeldata[i+2];
		j++;
		if(j==largura){
			k++;
			j=0;
		}
	}
}
void Imagem::escrever_novo_arquivo(){
	int i,j;
        novo_arquivo << "P6" << endl;
        novo_arquivo << largura << " " << altura << endl;
        novo_arquivo << numero_magico << endl;
       	for(i=0;i<altura;i++){
		for(j=0;j<largura;j++){
			novo_arquivo << pixel[i][j][0] << pixel[i][j][1] << pixel[i][j][2];
		}
	}
	
}

int Imagem::get_altura(){
        return altura;
}

int Imagem::get_largura(){
        return largura;
}

int Imagem::get_numero_magico(){
        return numero_magico;
}

int Imagem::get_tamanho(){
        return tamanho;
}

unsigned char*** Imagem::get_pixel(){
	return pixel;
}

void Imagem::set_pixel(unsigned char*** pixel){
	this->pixel=pixel;
}
int Imagem::get_open(){
	return open;
}
