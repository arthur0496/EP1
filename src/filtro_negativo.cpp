#include<iostream>
#include"filtro_negativo.hpp"

using namespace std;

Filtro_Negativo::Filtro_Negativo(int altura,int largura,int numero_magico, unsigned char*** pixel){
        set_altura(altura);
        set_largura(largura);
        set_numero_magico(numero_magico);
	set_pixel(pixel);
}
void Filtro_Negativo::aplicar_filtro(){
	int i,j,largura,altura;
        unsigned char*** pixel;

        largura=get_largura();
        altura=get_altura();
        pixel = get_pixel();
        for(i=0;i<altura;i++){
                for(j=0;j<largura;j++){
                        pixel[i][j][0] = numero_magico - pixel[i][j][0];
                        pixel[i][j][1] = numero_magico - pixel[i][j][1];
                        pixel[i][j][2] = numero_magico - pixel[i][j][2];
                }
        }
        set_pixel(pixel);
}

void Filtro_Negativo::set_numero_magico(int numero_magico){
	this->numero_magico = numero_magico;
}

int Filtro_Negativo::get_numero_magico(){
	return numero_magico;
}
