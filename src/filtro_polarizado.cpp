#include<iostream>
#include"filtro_polarizado.hpp"

using namespace std;

Filtro_Polarizado::Filtro_Polarizado(int altura,int largura,int numero_magico, unsigned char*** pixel){
        set_altura(altura);
        set_largura(largura);
        this->numero_magico = numero_magico;
	set_pixel(pixel);
}

void Filtro_Polarizado::aplicar_filtro(){
        int i,j,largura,altura;
        unsigned char*** pixel;

        largura=get_largura();
        altura=get_altura();
	pixel = get_pixel();
        for(i=0;i<altura;i++){
		for(j=0;j<largura;j++){
               		if(pixel[i][j][0] < numero_magico/2)
				pixel[i][j][0] = 0;
			else
				pixel[i][j][0] = 255;
			if(pixel[i][j][1] < numero_magico/2) 
                         	pixel[i][j][1] = 0;
                	else
                        	pixel[i][j][1] = 255;
			if(pixel[i][j][2] < numero_magico/2) 
                        	pixel[i][j][2] = 0;
                	else
                        	pixel[i][j][2] = 255;
		}
        }
	set_pixel(pixel);
}

int Filtro_Polarizado::get_numero_magico(){
	return numero_magico;
}

void Filtro_Polarizado::set_numero_magico(int numero_magico){
	this-> numero_magico = numero_magico;
}
