#ifndef FILTRO_POLARIZADO_HPP
#define FILTRO_POLARIZADO_HPP

#include<iostream>
#include"filtro.hpp"

using namespace std;

class Filtro_Polarizado : public Filtro {
private:
	int numero_magico;
public:
        Filtro_Polarizado(int altura, int largura,int numero_magico,unsigned char*** pixel);
        void aplicar_filtro();
	int get_numero_magico();
	void set_numero_magico(int numero_magico);
};

#endif

