#ifndef FILTRO_NEGATIVO_HPP
#define FILTRO_NEGATIVO_HPP

#include<iostream>
#include"filtro.hpp"

using namespace std;

class Filtro_Negativo : public Filtro {
private:
	int numero_magico;
public:
	Filtro_Negativo(int altura, int largura,int numero_magico,unsigned char*** pixel);
	void aplicar_filtro();
	int get_numero_magico();
	void set_numero_magico(int numero_magico);
};

#endif
