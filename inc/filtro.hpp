#ifndef FILTRO_HPP
#define FILTRO_HPP

#include <iostream>

using namespace std;

class Filtro{

private:
	int altura,largura;
	unsigned char ***pixel;

public:
	Filtro();
	Filtro(int altura,int largura, unsigned char*** pixel);
	void aplicar_filtro();
	int get_altura();
	int get_largura();
	unsigned char*** get_pixel();
	void set_altura(int altura);
	void set_largura(int largura);
	void set_pixel(unsigned char*** pixel);
};

#endif
