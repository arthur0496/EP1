#ifndef FILTRO_MEDIA_HPP
#define FILTRO_MEDIA_HPP

#include<iostream>
#include"filtro.hpp"

using namespace std;

class Filtro_Media : public Filtro{
public:
        Filtro_Media(int altura, int largura,unsigned char*** pixel);
	void aplicar_filtro();
};

#endif
