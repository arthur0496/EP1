#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <fstream>

using namespace std;

class Imagem {

private:

	int altura,largura,numero_magico,tamanho;
	int open;
	ifstream arquivo;
	ofstream novo_arquivo;
		char* pixeldata;
	unsigned char ***pixel;
	
public:

	Imagem();
	~Imagem();
	void abrir_arquivo();
	void ler_arquivo();
	void criar_novo_arquivo();
	void escrever_novo_arquivo();
	int get_altura();
	int get_largura();
	int get_numero_magico();
	int get_tamanho();
	unsigned char*** get_pixel();
	void set_pixel(unsigned char*** pixel);
	int get_open();
};

#endif
