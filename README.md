	Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

 -Abra o terminal;
 -Encontre o diretório raiz do projeto; 
 -Limpe os arquivos objeto: $ make clean
 -Compile o programa: $ make
 -Execute: $ make run

	Para executar o programa a imagem na qual deseja aplicar o filtro deve estar no diretório "EP0/doc/", e a imagem criada com o filtro será criada no mesmo diretório.

	Quando o programa for executado ele pedira para que digite o nome da imagem na qual deseja aplicar o filtro(sem extensão de formato), em seguida apresentara um menu com opções de filtro e o usuário deve digitar o numero correspondente ao filtro desejado, em seguida ele devera digitar o no me da imagem que será criada(sem extensão de formato) com o filtro e em seguida será perguntado se o usurário deseja realizar outro processo.
